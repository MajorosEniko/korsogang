using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateController : MonoBehaviour
{
    [SerializeField] 
    private Animator animator;
    [SerializeField]
    private bool isWalking;
  

    private int isWalkingHash;


    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();

        isWalkingHash = Animator.StringToHash("isWalking");
        
    }

    // Update is called once per frame
    void Update()
    {
        // Walking
        if (!isWalking && Input.GetKey(KeyCode.W)) // KeyCode.W == "w"
        {
            isWalking = true;
            animator.SetBool(isWalkingHash, isWalking);
        }
        
        if (isWalking && !Input.GetKey(KeyCode.W)) // KeyCode.W == "w"
        {
            isWalking = false;
            animator.SetBool(isWalkingHash, isWalking);
        }
        
       
    }
}