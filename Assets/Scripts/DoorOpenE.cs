using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenE : MonoBehaviour
{
     private bool Pressed;
     private int State;
 
     private Quaternion EndRot1, EndRot2;
    // Start is called before the first frame update
    void Start()
    {
                 EndRot1 = Quaternion.Euler(0, 90, 0); // Could be also set from the Inspector
         EndRot2 = Quaternion.identity;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
             Pressed = true;
 
         if (Pressed) // Rotation
         {
             if (transform.rotation == (State == 0 ? EndRot1 : EndRot2)) // Did the rotation end?
             {
                 State ^= 1; // Inverts "State" from 0 to 1 and reverse
                 Pressed = false;
             } // Slerp Method Here
             else this.transform.rotation = Quaternion.Lerp(transform.rotation, State == 0 ? EndRot1 : EndRot2, Time.deltaTime * 5);
         }
    }
}
