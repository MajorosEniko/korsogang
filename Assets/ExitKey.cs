using UnityEngine;
using System.Collections;

public class ExitKey : MonoBehaviour {

    public bool inTrigger;

	void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                ExitDoor.exitKey = true;
                Destroy(this.gameObject);
            }
        }
    }

    void OnGUI()
    {
        if (inTrigger)
        {
            GUI.Box(new Rect(0, 0, 300, 25), "Nyomd meg az 'E' gombot a kulcs felvételéhez.");
        }
    }
}