using System;
using UnityEngine;
using System.Collections;


public class ExitDoor : MonoBehaviour {

    public static bool exitKey;
    public bool open;
    public bool close;
    public bool inTrigger;
    public GameObject EndScrn;
    

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (close)
            {
                if (exitKey)
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        
                        EndScrn.gameObject.SetActive(true);
                    
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    close = true;
                    open = false;
                }
            }
        }

        if (open)
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
        else
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, 90.0f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
    }

    void OnGUI()
    {
        if (inTrigger)
        {
           
            
                if (exitKey)
                {
                    GUI.Box(new Rect(0, 0, 300, 25), "Nyomd meg az 'E' gombot az ajtó kinyitásához.");
                }
                else
                {
                    GUI.Box(new Rect(0, 0, 300, 25), "Kulcs szükséges.");
                }
            
        }
    }
}
