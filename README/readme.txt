Csapatnév: Korsó Gang

Csapattagok: 
Majoros Enikő - programozás, cetlik és feliratok működése, easteregg, textúrák
Tulipán Máté - programozás, ajtók használata, endscreen
Benda Matteo - ötletadó, történetírás, feliratok, logikai rejtvény, zene

Játékleírás: Egy szabadulószoba jellegű, gondolkozós, first person-nézetű játék. Emberünk a pécsi TTK
épületében ragadt, néhány jel arra utal, hogy kiütötte magát valamivel. Viszont este buli lesz a haverokkal a kiskorsóban,
ahová mindenképp el kellene jutnia. Egy titkos jóakaró hátrahagyott falfeliratokat, és cetliket, melyek segítségére lesznek a
játékosnak abban, hogy sikeresen kijusson a TTK főbejáratán, és eljusson a kiskorsóba. 

Irányítás: 
mozgás irányokba: "w,a,s,d"
ugrás: "space"
ajtónyitás, kulcsfelvétel: "e"

Játékmenet: A játékosnak meg kell találnia 2 kulcsot, és 5 jelszórészletet, melyeket össze kell illeszteni olyan módon, hogy egy értelmes mondatot adjon ki.